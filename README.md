# ESB Handball

![App screenshot when connected](img/connected-screenshot.png)

## Important links

|          |                                                       |
| -------- | ----------------------------------------------------- |
| Website  | https://esbhandball.azurewebsites.net/                |
| Database | https://esbhandball.scm.azurewebsites.net/phpMyAdmin/ |


## Database

### MySQL connection string

This project uses the **environment variable** `MYSQLCONNSTR_localdb` to read the connection string for the MySQL database using the **Azure format** ([read more about it](./src/Helpers/AzureMySQL.cs))

Example:
```
Data Source=localhost:3306;User Id=root;Password=pwd;Database=localdb
```
