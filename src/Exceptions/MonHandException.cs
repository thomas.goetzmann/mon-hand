using System;

namespace src.Exceptions
{
    [System.Serializable]
    public class MonHandException : Exception
    {
        public MonHandException() { }
        public MonHandException(string message) : base(message) { }
        public MonHandException(string message, System.Exception inner) : base(message, inner) { }
        protected MonHandException(
            System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}