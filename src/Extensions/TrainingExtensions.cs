﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace mon_hand.Extensions
{
    public static class TrainingExtensions
    {
        public static IEnumerable<DayOfWeek> TrainingDays => new[] { DayOfWeek.Tuesday, DayOfWeek.Thursday };

        public static bool IsNotTrainingDay(this DayOfWeek day) => !TrainingDays.Contains(day);

        public static string ToFrenchString(this DayOfWeek day)
        {
            switch (day)
            {
                case DayOfWeek.Monday:
                    return "Lundi";
                case DayOfWeek.Tuesday:
                    return "Mardi";
                case DayOfWeek.Wednesday:
                    return "Mercredi";
                case DayOfWeek.Thursday:
                    return "Jeudi";
                case DayOfWeek.Friday:
                    return "Vendredi";
                case DayOfWeek.Saturday:
                    return "Samedi";
                case DayOfWeek.Sunday:
                    return "Dimanche";
                default:
                    throw new InvalidOperationException();
            }
        }
    }
}
