using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using mon_hand.Models;
using mon_hand.ViewModels;
using src.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace mon_hand.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TrainingsController : Controller
    {
        private readonly ITrainingService trainingService;
        private readonly IPresenceService presenceService;

        public TrainingsController(ITrainingService trainingService, IPresenceService presenceService)
        {
            this.trainingService = trainingService;
            this.presenceService = presenceService;
        }

        [HttpGet("Next/{amount}")]
        public IEnumerable<Training> NextTwoTrainings(int amount) => trainingService.NextTrainings(DateTime.Now, amount);

        [HttpGet("Info")]
        public IEnumerable<Training> GetTrainings()
        {
            return new[]{
                new Training{Start = new DateTime(1,1,2,20,0,0), End = new DateTime(1,1,1,22,0,0), Location = "Betschdorf"},
                new Training{Start = new DateTime(1,1,4,20,0,0), End = new DateTime(1,1,1,22,0,0), Location = "Betschdorf"}
            };
        }

        /// <summary>
        /// Players presence for the given training. User information is anonymized
        /// </summary>
        [HttpGet("{trainingId}/Presences")]
        public async Task<ActionResult<IList<TrainingPresenceViewModel>>> PresencesAtTraining(int trainingId)
        {
            try
            {
                var result = await presenceService.PresencesAtTraining(trainingId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Players presence for the given training. Including user information details.
        /// </summary>
        [Authorize]
        [HttpGet("{trainingId}/PresencesDetails")]
        public async Task<ActionResult<IList<TrainingPresenceViewModel>>> PresencesDetailsAtTraining(int trainingId)
        {
            try
            {
                var result = await presenceService.PresencesDetailsAtTraining(trainingId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
