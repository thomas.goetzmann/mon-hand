using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using mon_hand.Models;
using src.Services;
using src.ViewModels;

namespace src.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PresencesController : Controller
    {
        private readonly IPresenceService _presenceService;

        public PresencesController(IPresenceService presenceService)
        {
            _presenceService = presenceService;
        }

        [HttpPost]
        public async Task<ActionResult<TrainingPresence>> AddPresence([FromBody]TrainingPresence presence)
        {
            //TODO Use user id in Authorization header instead of user.id passed in parameter
            try
            {
                await _presenceService.CreateOrUpdateAsync(presence);
                return CreatedAtAction("AddPresence", presence);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("TrainingPresence")]
        public async Task<ActionResult<TrainingPresence>> Presence([FromBody]TrainingPresence trainingPresence)
        {
            try
            {
                var result = await _presenceService.GetPresence(trainingPresence.UserId, trainingPresence.TrainingId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}