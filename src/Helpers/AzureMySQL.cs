using System;
using System.Linq;

namespace src.Helpers
{
    public static class AzureMySQL
    {
        /// <summary>
        /// Converts Azure MySQL Connection String (environment variable 'MYSQLCONNSTR_localdb') to standard one (used by EntityFramework).
        ///     Azure:      "Database={0};Data Source={1}:{2};User Id={3};Password={4}"
        ///     Standard:   "server={1};user id={3};password={4};port={2};database={0}"
        /// </summary>
        public static string ToMySqlStandard(string azureSqlString)
        {
            if (string.IsNullOrWhiteSpace(azureSqlString))
                throw new ArgumentException("Connection String is empty.");

            var azureValues = azureSqlString.Split(';', StringSplitOptions.RemoveEmptyEntries);

            var azureDictionary = azureValues.Select(item => item.Split('='))
                                   .ToDictionary(s => s[0], s => s[1]);

            var splittedDataSource = azureDictionary["Data Source"].Split(':');

            var server = splittedDataSource[0];
            var port = splittedDataSource[1];
            var userId = azureDictionary["User Id"];
            var password = azureDictionary["Password"];
            var database = azureDictionary["Database"];

            return $"server={server};user id={userId};password={password};port={port};database={database};";
        }
    }
}