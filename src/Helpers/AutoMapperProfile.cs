using AutoMapper;
using mon_hand.Models;
using mon_hand.ViewModels;
using src.ViewModels;

namespace src.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<User, UserViewModel>()
                .ReverseMap();
            CreateMap<TrainingPresence, TrainingPresenceViewModel>();
                
        }
    }
}