﻿import React from 'react'
import { NavItem, NavLink } from "reactstrap";
import { Button, Form } from "react-bootstrap";
import { Link } from "react-router-dom";

const NavLoginButton = ({ user, onUserLogout }) => {
    if (user === null) {
      return (
        <NavItem>
          <NavLink tag={Link} className="text-dark" to="/login">
            Se connecter
          </NavLink>
        </NavItem>
      );
    } else {
      return (
        <Form onSubmit={() => onUserLogout(null)}>
          <Button type="submit" className="btn btn-danger">
            Se déconnecter
          </Button>
        </Form>
      );
    }
}

export default NavLoginButton
