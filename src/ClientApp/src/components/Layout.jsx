import React from "react";
import { Container } from "reactstrap";
import { NavMenu } from "./navigation/NavMenu";

const Layout = ({ children, user, onUserLogout }) => 
  <div>
    <NavMenu user={user} onUserLogout={onUserLogout} />
    <Container>{children}</Container>
  </div>

export default Layout
