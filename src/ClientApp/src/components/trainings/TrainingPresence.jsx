﻿import React, { useEffect, useState } from "react";
import { ToggleButton, ToggleButtonGroup } from "react-bootstrap";

const TrainingPresence = ({ training, user }) => {

  const [presence, setPresence] = useState("");

  const handlePresenceChange = value => {
    fetch("api/Presences", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + user.token
      },
      body: JSON.stringify({
        TrainingId: training.id,
        Presence: value,
        UserId: user.id
      })
    });

    setPresence(value)
  };

  const getTrainingPresence = async () =>
    await fetch("api/Presences/trainingPresence", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        Authorization: "Bearer " + user.token
      },
      body: JSON.stringify({
        userId: user.id,
        trainingId: training.id
      })
    })
      .then(response => response.json());

  useEffect(() => {
    if (training && user) {
      getTrainingPresence()
        .then(trainingPresence => setPresence(trainingPresence.presence));
    }
  }, [training, user])

  if (!user)
    return <div />

  return (
    <div>
      <ToggleButtonGroup type="radio" name="radio" className="mt-3" value={presence} onChange={handlePresenceChange}>
        <ToggleButton type="radio" size="sm" variant="outline-success" value="present">Présent</ToggleButton>
        <ToggleButton type="radio" size="sm" variant="outline-warning" value="absent">Absent</ToggleButton>
        <ToggleButton type="radio" size="sm" variant="outline-danger" value="injured">Blessé</ToggleButton>
      </ToggleButtonGroup>
    </div >
  );
}

export default TrainingPresence
