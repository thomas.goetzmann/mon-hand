import React, { Component } from "react";
import NumberUpDown from "../utils/NumberUpDown";
import Training from "./Training";
require("datejs");

export default class Trainings extends Component {
  state = {
    trainings: [],
    loadingTrainings: true
  };

  minTrainings = 1;
  maxTrainings = 20;
  initialTrainings = 3;

  componentDidMount() {
    this.loadTrainings(this.initialTrainings);
  }

  loadTrainings = amount => {
    if (amount !== "" && amount !== this.state.trainingsShown && amount <= this.maxTrainings && amount >= this.minTrainings) {
      //Todo handle remove instead of refetch when amount is lover
      this.setState({ loadingTrainings: true });
      fetch(`api/Trainings/Next/${amount}`)
        .then(res => res.json())
        .then(trainings => this.setState({ trainings: trainings, loadingTrainings: false }));
    }
  };

  render() {
    return (
      <div>
        <h3>Prochains entrainements</h3>
        {this.state.loadingTrainings ? (
          <p>
            <em>En chargement des entrainements...</em>
          </p>
        ) : (
          <div>
            <NumberUpDown min={this.minTrainings} max={this.maxTrainings} defaultValue={this.state.trainings.length} handleChange={this.loadTrainings} />
            <ul className="list-group">
              {this.state.trainings.map(training => (
                <li key={training.id} className="list-group-item">
                  <Training training={training} user={this.props.user} />
                </li>
              ))}
            </ul>
          </div>
        )}
      </div>
    );
  }
}
