﻿import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck, faTimes, faUserInjured } from "@fortawesome/free-solid-svg-icons";
import { Button, OverlayTrigger, Tooltip } from "react-bootstrap";

const Presences = ({ trainingId, user }) => {

  const [presences, setPresences] = useState([]);
  const [counter, setCounter] = useState({present: null, absent: null, injured: null})

  const showPresenceDetails = () => { /*TODO*/ };
  const tooltip = `${counter.present} présents, ${counter.absent} absents, ${counter.injured} blessés.`;

  useEffect(() => {
    if (user) {
      fetch(`api/Trainings/${trainingId}/PresencesDetails`, {
        method: "GET",
        headers: {
          Authorization: "Bearer " + user.token
        }
      })
        .then(response => response.json())
        .then(p => {
          setPresences(p);
          setCounter({
            present: p.filter(x => x.presence === "present").length,
            absent: p.filter(x => x.presence === "absent").length,
            injured: p.filter(x => x.presence === "injured").length
          })
        }
        );
    }
    else {
      fetch(`api/Trainings/${trainingId}/Presences`)
        .then(response => response.json())
        .then(p => {
          setPresences(p);
          setCounter({
            present: p.filter(x => x.presence === "present").length,
            absent: p.filter(x => x.presence === "absent").length,
            injured: p.filter(x => x.presence === "injured").length
          })
        });
    }

    return () => { }
  }, [trainingId, user]);

  return <span>
    <OverlayTrigger overlay={<Tooltip id={`tooltip-${trainingId}`}>{tooltip}</Tooltip>}>
      <Button onClick={showPresenceDetails} size="sm" variant="outline-secondary">
        {counter.present}<FontAwesomeIcon icon={faCheck} color="green" />{' '}
        {counter.absent}<FontAwesomeIcon icon={faTimes} color="orange" />{' '}
        {counter.injured}<FontAwesomeIcon icon={faUserInjured} color="red" />{'   '}
      </Button>
    </OverlayTrigger>
  </span>
}

export default Presences
