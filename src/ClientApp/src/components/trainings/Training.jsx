import React from "react";
import TrainingPresence from "./TrainingPresence"
import Presences from "./Presences"

const Training = ({ training, user }) =>
  <div>
    <div>
      <span>
        {training.dayOfWeekFrench} {Date.parse(training.start).toString("dd/MM")} à{" "}
        <a href={training.location === "Betschdorf" ? "https://goo.gl/maps/bYG2UHwRHFs" : "https://goo.gl/maps/WRWHiwuBJ3q"}>{training.location}</a>,
      </span>
      <span> de {Date.parse(training.start).toString("HH:mm")}</span>
      <span> à {Date.parse(training.end).toString("HH:mm")}   </span>
      <Presences trainingId={training.id} user={user} />
    </div>
    <TrainingPresence training={training} user={user} />
  </div>

export default Training
