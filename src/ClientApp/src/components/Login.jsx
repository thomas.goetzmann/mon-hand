import React, { Component } from "react";
import { Form, Button, Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEnvelope, faLock } from "@fortawesome/free-solid-svg-icons";
import Hash from "hash.js";
import "../css/Login.css";

export default class Login extends Component {

  state = {
    credentials: {
      email: "",
      password: ""
    },
    error: ""
  };

  validateForm() {
    const { credentials } = this.state;

    return credentials.email.length > 0
        && credentials.password.length >= 0;
  }

  handleChange = event => {
    const { id, value } = event.target;
    const { credentials } = this.state;
    this.setState({
      credentials: {
        ...credentials,
        [id]: value
      },
      error: ""
    });
  };

  handleLogout = () => {
    this.props.onUserLogin(null);
  };

  handleLogin = event => {
    event.preventDefault();

    var user = { ...this.state.credentials };
    //Hashing the password doesn't ensure security but avoids sending it in cleartext from client to server (in case it's used somewhere else by the user)
    //Security should be built in server side !
    user.password = Hash.sha256()
      .update(user.password)
      .digest("hex");

    fetch("api/Users/authenticate", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(user)
    })
    .then(response => {
      if (!response.ok) {
        throw new Error(response.status);
      }
      response
        .json()
        .then(user => this.props.onUserLogin(user))
        .then(this.props.history.push("/"));
    })
    .catch(error => {
      this.setState({ error: error + " - La connexion a échoué." });
    });
  };

  render() {
    if (this.props.user == null) {
      return this.renderLogin();
    } else {
      return this.renderLogout();
    }
  }

  renderLogin() {
    const { credentials, error } = this.state;
    return (
      <div className="Login">
        <Form onSubmit={this.handleLogin}>
          <Form.Group as={Row} controlId="email" bssize="large">
            <Col sm="2" className="Login-icon">
              <FontAwesomeIcon icon={faEnvelope} size="lg" />
            </Col>
            <Col sm="10">
              <Form.Control type="email" value={credentials.email} onChange={this.handleChange} placeholder="Email" />
            </Col>
          </Form.Group>
          <Form.Group as={Row} controlId="password" bssize="large">
            <Col sm="2" className="Login-icon">
              <FontAwesomeIcon icon={faLock} size="lg" />
            </Col>
            <Col sm="10">
              <Form.Control value={credentials.password} onChange={this.handleChange} type="password" placeholder="Mot de passe" />
            </Col>
          </Form.Group>
          <div className={"ErrorMessage " + error !== null && error !== "" ? "alert alert-danger" : ""}>{error}</div>
          <Button block bssize="large" disabled={!this.validateForm()} type="submit">
            Se connecter
          </Button>
          <Link to="/registration" className="Login-link btn btn-outline-primary">
            S'inscrire
          </Link>
        </Form>
      </div>
    );
  }

  renderLogout() {
    const { firstName, lastName, email } = this.props.user;

    return (
      <p class="alert alert-warning">
        Vous êtes déjà connecté en tant que '
        <b>
          {firstName} {lastName}
        </b>
        ' avec l'email '<b>{email}</b>'.
      </p>
    );
  }
}
