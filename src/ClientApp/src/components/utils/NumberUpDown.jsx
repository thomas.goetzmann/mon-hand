import React, { useState } from "react";

const NumberUpDown = ({ handleChange, min, max, defaultValue}) => {

  const [value, setValue] = useState(defaultValue)

  return (
    <div className="input-group input-group-sm mb-3">
      <div className="input-group-prepend">
        <button
          type="button"
          className="btn btn-secondary"
          data-toggle="tooltip"
          data-placement="top"
          id="nextTrainingsShownButton"
          onClick={() => handleChange(value)}
          title="Cliquer pour afficher les prochains entrainements"
        >
          Afficher
        </button>
      </div>
      <input
        type="number"
        min={min}
        max={max}
        aria-describedby="nextTrainingsShownButton"
        value={value}
        onChange={evt => setValue(evt.target.value)}
      />
    </div>
  );
}

export default NumberUpDown
