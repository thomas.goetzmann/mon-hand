import React from "react";
import TrainingsGeneralInfos from "./generalInfos/TrainingsGeneralInfos";
import Trainings from "./trainings/Trainings";

const Main = props => 
    <div>
      <h1>MonHand</h1>
      <Trainings user={props.user} />
      <TrainingsGeneralInfos />
    </div>

export default Main;
