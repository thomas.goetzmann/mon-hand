﻿import React, { Component } from "react";
import { Form, Button, Row, Col } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUser, faUserTag, faEnvelope, faLock } from "@fortawesome/free-solid-svg-icons";
import Hash from "hash.js";
import "../css/Registration.css";

export class Registration extends Component {
  state = {
    user: {
      email: "",
      firstName: "",
      lastName: "",
      password: ""
    },
    status: ""
  };

  validateForm() {
    const { user } = this.state;
    return user.email.length > 0 && user.firstName.length > 0 && user.lastName.length > 0 && user.password.length >= 4;
  }

  handleChange = event => {
    const { id, value } = event.target;
    const { user } = this.state;
    this.setState({
      user: {
        ...user,
        [id]: value
      }
    });
  };

  handleSubmit = event => {
    event.preventDefault();
    this.setState({ status: "" });

    var user = { ...this.state.user };
    //Hashing the password doesn't ensure security but avoids sending it in cleartext from client to server (in case it's used somewhere else by the user)
    //Security should be built in server side !
    user.password = Hash.sha256()
      .update(user.password)
      .digest("hex");

    fetch("api/Users/register", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(user)
    })
      .then(response => {
        if (!response.ok) {
          throw new Error(response.status);
        }
        this.props.history.push("/login");
      })
      .catch(error => {
        this.setState({ status: error + " - Vérifiez vos informations et assurez vous que votre email n'a pas déjà été utilisé." });
      });
  };

  componentWillUnmount() {
    // This is just necessary in the case that the screen is closed before the timeout fires, otherwise it would cause a memory leak that would trigger the transition regardless, breaking the user experience.
    clearTimeout(this.timeoutHandle);
  }

  render() {
    const { user, status } = this.state;
    return (
      <div className="Registration">
        <Form onSubmit={this.handleSubmit}>
          <Form.Group as={Row} controlId="email" bssize="large">
            <Col sm="2" className="Registration-icon">
              <FontAwesomeIcon icon={faEnvelope} size="lg" />
            </Col>
            <Col sm="10">
              <Form.Control type="email" value={user.email} onChange={this.handleChange} placeholder="Email" />
            </Col>
          </Form.Group>
          <Form.Group as={Row} controlId="firstName" bssize="large">
            <Col sm="2" className="Registration-icon">
              <FontAwesomeIcon icon={faUser} size="lg" />
            </Col>
            <Col sm="10">
              <Form.Control autoFocus type="plaintext" value={user.firstName} onChange={this.handleChange} placeholder="Prénom" />
            </Col>
          </Form.Group>
          <Form.Group as={Row} controlId="lastName" bssize="large">
            <Col sm="2" className="Registration-icon">
              <FontAwesomeIcon icon={faUserTag} size="lg" />
            </Col>
            <Col sm="10">
              <Form.Control autoFocus type="plaintext" value={user.lastName} onChange={this.handleChange} placeholder="Nom de famille" />
            </Col>
          </Form.Group>

          <Form.Group as={Row} controlId="password" bssize="large">
            <Col sm="2" className="Registration-icon">
              <FontAwesomeIcon icon={faLock} size="lg" />
            </Col>
            <Col sm="10">
              <Form.Control value={user.password} onChange={this.handleChange} type="password" placeholder="Mot de passe" />
            </Col>
          </Form.Group>
          <div className={"ErrorMessage " + status !== null && status !== "" ? "alert alert-danger" : ""}>{status}</div>
          <Button block disabled={!this.validateForm()} type="submit">
            S'inscrire
          </Button>
        </Form>
      </div>
    );
  }
}
