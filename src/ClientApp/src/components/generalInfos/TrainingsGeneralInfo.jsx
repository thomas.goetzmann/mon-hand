﻿import React from "react";

const TrainingsGeneralInfo = ({ info }) =>
  <div>
    <strong>{info.dayOfWeekFrench}</strong> de
    <strong>
      {" "}
      {Date.parse(info.start).toString("HH:mm")} à {Date.parse(info.end).toString("HH:mm")}{" "}
    </strong>
    toujours à {info.location} (sauf cas exceptionnel)
  </div>

export default TrainingsGeneralInfo
