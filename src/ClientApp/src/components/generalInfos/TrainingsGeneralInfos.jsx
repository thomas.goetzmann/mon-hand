import React, { useEffect, useState } from "react";
import TrainingsGeneralInfo from "./TrainingsGeneralInfo"

const TrainingsGeneralInfos = props => {

  //Hardcoded values allow faster page loading. Remove state initialization and uncomment the useEffect to get data from the API
  const [infos, setInfos] = useState([
    { dayOfWeekFrench: "Mardi", start: "0001-01-02T20:00:00", end: "0001-01-02T20:00:00", location: "Betschdorf" },
    { dayOfWeekFrench: "Jeudi", start: "0001-01-04T20:00:00", end: "0001-01-04T20:00:00", location: "Betschdorf" }
  ]);

  //useEffect(() => {
  //  fetch("api/Trainings/Info")
  //    .then(res => res.json())
  //    .then(infos => { setInfos(infos); console.log(infos) });
  //}, []);
 
  return (
    <div className="shadow bg-light rounded">
      <h3 className="mt-4">Informations</h3>
      <p>L'ASB handball s'entraine chaque:</p>
      <ul>
        {infos.map((info,index) => (
          <li key={index}>
            <TrainingsGeneralInfo info={info} />
          </li>
        ))}
      </ul>
    </div>
  );
};

export default TrainingsGeneralInfos;
