import React, { Component } from "react";
import { Route } from "react-router";
import Layout from "./components/Layout";
import Main from "./components/Main";
import { Registration } from "./components/Registration";
import Login from "./components/Login";

export default class App extends Component {
  static displayName = App.name;

  state = { user: JSON.parse(localStorage.getItem("user")) };

  handleUserLog = user => {
    if (user != null) {
      localStorage.setItem("user", JSON.stringify(user));
      localStorage.setItem("userDate", Date.now());
    } else {
      localStorage.removeItem("user");
      localStorage.removeItem("userDate");
    }
    this.setState({ user });
  };

  render() {
    const main = () => <Main user={this.state.user} {...this.props} />

    return (
      <Layout user={this.state.user} onUserLogout={this.handleUserLog}>
        <Route path="/" exact render={main} />
        <Route path="/registration" exact component={Registration} />
        <Route path="/login" exact render={props => <Login {...props} onUserLogin={this.handleUserLog} user={this.state.user} />} />
      </Layout>
    );
  }
}
