namespace mon_hand.Models
{
    public class TrainingPresence
    {
        public int UserId { get; set; }
        public User User { get; set; }
        public int TrainingId { get; set; }
        public Training Training { get; set; }
        public string Presence { get; set; }
    }
}