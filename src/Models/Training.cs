using mon_hand.Extensions;
using System;
using System.Collections.Generic;

namespace mon_hand.Models
{
    public class Training
    {
        public int Id { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string Location { get; set; }
        public ICollection<TrainingPresence> Presences { get; } = new List<TrainingPresence>();

        public DayOfWeek DayOfWeek => Start.DayOfWeek;
        public string DayOfWeekFrench => Start.DayOfWeek.ToFrenchString();
    }
}
