﻿using Microsoft.EntityFrameworkCore;
using Pomelo.EntityFrameworkCore.MySql.Infrastructure;
using src.Helpers;
using System;

namespace mon_hand.Models
{
    public class EsbHandballDbContext : DbContext
    {
        public EsbHandballDbContext()
        {
        }

        public EsbHandballDbContext(DbContextOptions options) : base(options)
        {
        }

        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Training> Trainings { get; set; }
        public virtual DbSet<TrainingPresence> TrainingPresences { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            if (!optionsBuilder.IsConfigured)
            {
                //The connnection string should look like: "server=localhost;user id=root;password=password;port=3306;database=esbhandball_db;";
                var dbConnectionString = Environment.GetEnvironmentVariable("MYSQLCONNSTR_localdb");

                optionsBuilder.UseMySql(
                    AzureMySQL.ToMySqlStandard(dbConnectionString), 
                    mysqloptions => { mysqloptions.ServerVersion(new Version(8, 0, 13), ServerType.MySql); }
                );
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<User>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Email).IsRequired();
                entity.Property(e => e.Password).IsRequired();
                entity.HasIndex(e => e.Email).IsUnique();
                entity.HasMany(e => e.Presences).WithOne(p => p.User).OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<Training>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Start).IsRequired();
                entity.Property(e => e.End).IsRequired();
                entity.HasMany(e => e.Presences).WithOne(p => p.Training).OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<TrainingPresence>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.TrainingId });
                entity.Property(e => e.Presence).IsRequired();
                entity.HasOne(e => e.User).WithMany(u => u.Presences).HasForeignKey(e => e.UserId).IsRequired();
                entity.HasOne(e => e.Training).WithMany(t => t.Presences).HasForeignKey(e => e.TrainingId).IsRequired();
            });
        }
    }
}
