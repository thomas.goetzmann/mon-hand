﻿using System.Collections.Generic;

namespace mon_hand.Models
{
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public ICollection<TrainingPresence> Presences { get; } = new List<TrainingPresence>();
    }
}
