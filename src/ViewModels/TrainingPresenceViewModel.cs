﻿using mon_hand.Models;
using src.ViewModels;

namespace mon_hand.ViewModels
{
    public class TrainingPresenceViewModel
    {
        public int UserId { get; set; }
        public UserViewModel User { get; set; }
        public int TrainingId { get; set; }
        public Training Training { get; set; }
        public string Presence { get; set; }
    }
}
