using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using mon_hand.Models;
using src.Exceptions;

namespace src.Services
{
    public interface IUserService
    {
        User Authenticate(string email, string password);
        Task<List<User>> GetAllAsync();
        User GetById(int id);
        Task<User> GetByIdAsync(int id);
        Task<int> CreateAsync(User user);
        Task UpdateAsync(User user);
        Task DeleteAsync(User user);
    }

    public class UserService : IUserService
    {
        private readonly EsbHandballDbContext dbContext;

        public UserService(EsbHandballDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public User Authenticate(string email, string password)
        {
            if (string.IsNullOrEmpty(email) || string.IsNullOrEmpty(password))
                throw new LoginException("Login information cannot be empty. Please enter a valid email and password.");

            var user = dbContext.Users.SingleOrDefault(u => u.Email.Equals(email, StringComparison.CurrentCultureIgnoreCase));

            if (user == null)
                throw new LoginException($"Email '{email}' not found. Please verify your email or register before login.");

            if (user.Password != ToSha256(password))
                throw new LoginException($"Invalid password for '{email}'.");

            return user;
        }

        private string ToSha256(string value)
        {
            StringBuilder hash = new StringBuilder();
            using (var mySha256 = SHA256.Create())
            {
                foreach (var b in mySha256.ComputeHash(Encoding.UTF8.GetBytes(value)))
                    hash.Append(b.ToString("x2"));
            }
            return hash.ToString();
        }

        public Task<List<User>> GetAllAsync()
        {
            return dbContext.Users.ToListAsync();
        }

        public User GetById(int id)
        {
            return dbContext.Users.Find(id);
        }

        public Task<User> GetByIdAsync(int id)
        {
            return dbContext.Users.FindAsync(id);
        }

        public Task<int> CreateAsync(User user)
        {
            ValidateForCreation(user);
            user.Password = ToSha256(user.Password);
            dbContext.Users.Add(user);
            return dbContext.SaveChangesAsync();
        }

        private void ValidateForCreation(User user)
        {
            ValidateForUpdate(user);
            if (dbContext.Users.Any(u => u.Email.Equals(user.Email, StringComparison.CurrentCultureIgnoreCase)))
                throw new MonHandException($"Email '{user.Email}' is already registered.");
        }

        private void ValidateForUpdate(User user)
        {
            if (string.IsNullOrEmpty(user.Password))
                throw new MonHandException("Password is required");

            if (string.IsNullOrEmpty(user.Email))
                throw new MonHandException("Email is required");
        }

        public Task UpdateAsync(User user)
        {
            ValidateForUpdate(user);
            user.Password = ToSha256(user.Password);
            dbContext.Entry(user).State = EntityState.Modified;
            try
            {
                return dbContext.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(user.Id))
                    throw new NotFoundException($"User with email {user.Email} not found");
                else
                    throw;
            }
        }

        private bool UserExists(int id)
        {
            return dbContext.Users.Any(e => e.Id == id);
        }

        public Task DeleteAsync(User user)
        {
            dbContext.Users.Remove(user);
            return dbContext.SaveChangesAsync();
        }
    }
}