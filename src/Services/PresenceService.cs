using AutoMapper;
using Microsoft.EntityFrameworkCore;
using mon_hand.Models;
using mon_hand.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace src.Services
{
    public interface IPresenceService
    {
        Task<int> CreateOrUpdateAsync(TrainingPresence presence);
        Task<TrainingPresence> GetPresence(int userId, int trainingId);
        Task<IList<TrainingPresenceViewModel>> PresencesDetailsAtTraining(int trainingId);
        Task<IList<TrainingPresenceViewModel>> PresencesAtTraining(int trainingId);
    }

    public class PresenceService : IPresenceService
    {
        private readonly EsbHandballDbContext dbContext;
        private readonly IMapper mapper;

        public PresenceService(EsbHandballDbContext dbContext, IMapper mapper)
        {
            this.dbContext = dbContext;
            this.mapper = mapper;
        }

        public Task<int> CreateOrUpdateAsync(TrainingPresence presence)
        {
            var dbPresence = dbContext.TrainingPresences.Find(presence.UserId, presence.TrainingId);

            if (dbPresence == null)
                dbContext.TrainingPresences.Add(presence);
            else
                dbPresence.Presence = presence.Presence;

            return dbContext.SaveChangesAsync();
        }

        public async Task<TrainingPresence> GetPresence(int userId, int trainingId)
        {
            var result = await dbContext.TrainingPresences.FindAsync(userId, trainingId);

            if (result == null)
                return new TrainingPresence();

            return new TrainingPresence
            {
                Presence = result.Presence,
                TrainingId = result.TrainingId,
                UserId = result.UserId,
            };
        }

        public async Task<IList<TrainingPresenceViewModel>> PresencesDetailsAtTraining(int trainingId)
        {
            var result = await dbContext
                .TrainingPresences
                .Include(tp => tp.User)
                .Where(p => p.TrainingId == trainingId)
                .Select(p => mapper.Map<TrainingPresenceViewModel>(p))
                .ToListAsync();

            if (result == null)
                return new List<TrainingPresenceViewModel>();

            return result;
        }

        public async Task<IList<TrainingPresenceViewModel>> PresencesAtTraining(int trainingId)
        {
            var result = await dbContext
                .TrainingPresences
                .Where(p => p.TrainingId == trainingId)
                .Select(p => new TrainingPresenceViewModel { Presence = p.Presence, TrainingId = p.TrainingId })
                .ToListAsync();

            if (result == null)
                return new List<TrainingPresenceViewModel>();

            return result;
        }
    }
}