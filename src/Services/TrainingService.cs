using mon_hand.Extensions;
using mon_hand.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace src.Services
{
    public interface ITrainingService
    {
        Training NextTraining(DateTime now);
        string TrainingLocationOn(DateTime day);
        IEnumerable<Training> NextTrainings(DateTime now, int amount);

        IEnumerable<Training> NextTrainingsWithUserPresence(DateTime now, int amount, User user);
    }

    public class TrainingService : ITrainingService
    {
        private readonly EsbHandballDbContext dbContext;

        public TrainingService(EsbHandballDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public Training NextTraining(DateTime now)
        {
            var nextTraining = now;
            while (nextTraining.DayOfWeek.IsNotTrainingDay())
            {
                nextTraining = nextTraining.AddDays(1);
            }

            var start = new DateTime(nextTraining.Year, nextTraining.Month, nextTraining.Day, 20, 00, 00);
            var end = new DateTime(nextTraining.Year, nextTraining.Month, nextTraining.Day, 22, 00, 00);
            var location = TrainingLocationOn(start);

            return new Training { Start = start, End = end, Location = location };
        }

        public string TrainingLocationOn(DateTime day)
        {
            if (day.DayOfWeek != DayOfWeek.Thursday)
                return "Betschdorf";

            var weekNumber = CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(day, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            if (weekNumber % 2 == 0)
                return "Betschdorf";
            else
                return "Seltz";
        }

        public IEnumerable<Training> NextTrainings(DateTime now, int amount)
        {
            for (int i = 0; i < amount; i++)
            {
                var nextTraining = NextTraining(now);
                now = nextTraining.End.AddDays(1);
                yield return EnsureSavedInDatabase(nextTraining, null);
            }
        }

        public IEnumerable<Training> NextTrainingsWithUserPresence(DateTime now, int amount, User user)
        {
            for (int i = 0; i < amount; i++)
            {
                var nextTraining = NextTraining(now);
                now = nextTraining.End.AddDays(1);
                yield return EnsureSavedInDatabase(nextTraining, user);
            }
        }

        private Training EnsureSavedInDatabase(Training training, User user)
        {
            Training trainingInDatabase;
            if (user != null)
            {
                trainingInDatabase = dbContext.TrainingPresences.Where(p => p.Training.Start == training.Start && p.User.Email == user.Email)
                                                                .Include(p => p.User)
                                                                .Include(p => p.Training)
                                                                .Select(p => p.Training)
                                                                .FirstOrDefault();
            }
            else
            {
                trainingInDatabase = dbContext.Trainings.FirstOrDefault(t => t.Start == training.Start);
            }

            if (trainingInDatabase != null)
            {
                return trainingInDatabase;
            }
            else
            {
                dbContext.Trainings.Add(training);
                dbContext.SaveChanges();
                return training;
            }
        }
    }
}
