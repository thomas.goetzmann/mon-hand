using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using mon_hand.Extensions;
using mon_hand.Models;
using src.Services;
using System;

namespace mon_hand.tests.csproj
{
    [TestClass]
    public class TrainingTests
    {
        private ITrainingService IndependentTrainingService(EsbHandballDbContext dbContext = null) 
            => new TrainingService( 
                dbContext ?? new EsbHandballDbContext());

        [TestMethod]
        public void GetNextTrainings_OnMonday_ShouldBeTuesday()
        {
            //Arrange
            var monday = new DateTime(2019, 01, 07);
            monday.DayOfWeek.Should().Be(DayOfWeek.Monday);
            var trainingService = IndependentTrainingService();

            //Act
            var nextTraining = trainingService.NextTraining(monday);

            //Assert
            nextTraining.DayOfWeek.Should().Be(DayOfWeek.Tuesday);
        }

        [TestMethod]
        public void GetNextTrainings_OnTuesday_ShouldBeTuesday()
        {
            //Arrange
            var tuesday = new DateTime(2019, 04, 02);
            tuesday.DayOfWeek.Should().Be(DayOfWeek.Tuesday);
            var trainingService = IndependentTrainingService();

            //Act
            var nextTraining = trainingService.NextTraining(tuesday);

            //Assert
            nextTraining.DayOfWeek.Should().Be(DayOfWeek.Tuesday);
        }

        [TestMethod]
        public void GetNextTrainings_OnWednesday_ShouldBeThursday()
        {
            //Arrange
            var wednesday = new DateTime(2019, 01, 02);
            wednesday.DayOfWeek.Should().Be(DayOfWeek.Wednesday);
            var trainingService = IndependentTrainingService();

            //Act
            var nextTraining = trainingService.NextTraining(wednesday);

            //Assert
            nextTraining.DayOfWeek.Should().Be(DayOfWeek.Thursday);
        }

        [TestMethod]
        public void GetNextTrainings_OnThursday_ShouldBeThursday()
        {
            //Arrange
            var thursday = new DateTime(2019, 02, 14);
            thursday.DayOfWeek.Should().Be(DayOfWeek.Thursday);
            var trainingService = IndependentTrainingService();

            //Act
            var nextTraining = trainingService.NextTraining(thursday);

            //Assert
            nextTraining.DayOfWeek.Should().Be(DayOfWeek.Thursday);
        }

        [TestMethod]
        public void GetNextTrainings_OnFriday_ShouldBeTuesday()
        {
            //Arrange
            var tuesday = new DateTime(2019, 01, 25);
            tuesday.DayOfWeek.Should().Be(DayOfWeek.Friday);
            var trainingService = IndependentTrainingService();

            //Act
            var nextTraining = trainingService.NextTraining(tuesday);

            //Assert
            nextTraining.DayOfWeek.Should().Be(DayOfWeek.Tuesday);
        }

        [TestMethod]
        public void GetNextTrainings_ThursdayUnEvenWeek_ShouldBeInSeltz()
        {
            //Arrange
            var thursdayUnevenWeek = new DateTime(2019, 01, 03);
            thursdayUnevenWeek.DayOfWeek.Should().Be(DayOfWeek.Thursday);
            var trainingService = IndependentTrainingService();

            //Act
            var nextTraing = trainingService.NextTraining(thursdayUnevenWeek);

            //Assert
            nextTraing.Location.Should().Be("Seltz");
        }

        [TestMethod]
        public void GetNextTrainings_ThursdayEvenWeek_ShouldBeInBetschdorf()
        {
            //Arrange
            var thursdayUnevenWeek = new DateTime(2019, 01, 10);
            thursdayUnevenWeek.DayOfWeek.Should().Be(DayOfWeek.Thursday);
            var trainingService = IndependentTrainingService();

            //Act
            var nextTraing = trainingService.NextTraining(thursdayUnevenWeek);

            //Assert
            nextTraing.Location.Should().Be("Betschdorf");
        }

        [TestMethod]
        public void ToFrenchString_ShouldBePascalCase()
        {
            DayOfWeek.Monday.ToFrenchString().Should().Be("Lundi");
            DayOfWeek.Tuesday.ToFrenchString().Should().Be("Mardi");
            DayOfWeek.Wednesday.ToFrenchString().Should().Be("Mercredi");
            DayOfWeek.Thursday.ToFrenchString().Should().Be("Jeudi");
            DayOfWeek.Friday.ToFrenchString().Should().Be("Vendredi");
            DayOfWeek.Saturday.ToFrenchString().Should().Be("Samedi");
            DayOfWeek.Sunday.ToFrenchString().Should().Be("Dimanche");
        }
    }
}
