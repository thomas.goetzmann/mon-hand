using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using src.Helpers;
using System;

namespace tests
{
    [TestClass]
    public class AzureMySqlTests
    {
        [DataTestMethod]
        [DataRow("Database=localdb;Data Source=127.0.0.1:3306;User Id=azure;Password=azurePassword",
                 "server=127.0.0.1;user id=azure;password=azurePassword;port=3306;database=localdb;")]
        [DataRow("Database=esbhandball_db;Data Source=localhost:3306;User Id=root;Password=password",
                 "server=localhost;user id=root;password=password;port=3306;database=esbhandball_db;")]
        public void ToMySqlStandard(string azure, string expectedResult)
        {
            //Act
            var result = AzureMySQL.ToMySqlStandard(azure);

            //Assert
            result.Should().Be(expectedResult);
        }
    }
}